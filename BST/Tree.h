#include <bits/stdc++.h>
using namespace std;
#define INF 100000000
#define val data
struct Node {
    int data;
    struct Node* left;
    struct Node* right;
    struct Node* nextRight;
    struct Node* next;
    Node(int x) {
        data = x;
        left = right = NULL;
    }
};

int height(Node* root) {
    if (!root) {
        return 0;
    }
    return 1 + max(height(root->left), height(root->right));
}