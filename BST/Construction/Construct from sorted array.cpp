#include "../Tree.h"

/**
 * https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/
 * Convert sorted array into height balanced BST = abs(height of left - height of right) <= 1 for every node
 * Take floor(n/2) that is the root
 * Recursively construct for left and right subtrees
 */
Node* createTree(vector<int>& inorder, int l, int r) {
    if (l > r || l < 0 || r >= inorder.size()) {
        return NULL;
    }
    int mid = (l + r) / 2;
    Node* root = new Node(inorder[mid]);
    root->left = createTree(inorder, l, mid - 1);
    root->right = createTree(inorder, mid + 1, r);
    return root;
}
Node* sortedArrayToBST(vector<int>& nums) {
    return createTree(nums, 0, nums.size() - 1);
}