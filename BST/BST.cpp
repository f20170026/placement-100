#include "Tree.h"

/**
 * Search, Insert, Delete all will take O(h) Time where h is height of BST.
 * In case of right or left askwed tree then it is O(n)
 * In case of balanced it is O(logn)
 */
bool search(Node *root, int key) {
    if (!root) {
        return false;
    }

    if (root->data == key) {
        return true;
    }

    if (key < root->data) {
        //Search left subtree
        return search(root->left, key);
    }

    else {
        //Search right subtree
        return search(root->right, key);
    }
}

Node *insertRecursive(Node *root, int key) {
    if (!root) {
        return new Node(key);
    }

    if (key < root->data) {
        root->left = insertRecursive(root->left, key);
    } else if (key > root->data) {
        root->right = insertRecursive(root->right, key);
    } else {
        //if key already present then we won't insert the node
        return root;
    }
}

Node *insertIterative(Node *root, int key) {
}

Node *inorderSuccessor(Node *node) {
    //go as much left as possible in right subtree
    node = node->right;
    while (node != NULL && node->left != NULL) {
        node = node->left;
    }
    return node;
}
Node *inorderPredecessor(Node *node) {
    //go as much right as possible in left subtree
    node = node->left;
    while (node != NULL && node->right != NULL) {
        node = node->right;
    }
    return node;
}

/**
 * In the worst case we will keep on going down the tree => O(h) Time
 * When root has two child why to replace with successor or predecessor?
 * 1 2 3 4 5 6
 * Say you want to delete 3 and it has two child.
 * You should replace it 4 (succ) or 2 (pred) to maintain sorted order.
 */

Node *deleteNode(Node *root, int key) {
    if (!root)
        return NULL;

    if (key < root->data)
        root->left = deleteNode(root->left, key);

    else if (key > root->data)
        root->right = deleteNode(root->right, key);

    else {
        if (root->left == NULL) {
            //replace root with root->right
            Node *rightSubtree = root->right;
            delete root;
            return rightSubtree;
        }

        if (root->right == NULL) {
            //replace root with root->left. return root->left to parent
            Node *leftSubtree = root->left;
            delete root;
            return leftSubtree;
        }
        //Need to delete root. Replace it with inorder successor or inorder predecesoor based on height

        //Delete from left subtree (replace with pred.) to reduce height
        if (height(root->left) > height(root->right)) {
            Node *pred = inorderPredecessor(root);
            root->data = pred->data;
            root->left = deleteNode(root->left, root->data);
        } else {
            Node *succ = inorderSuccessor(root);
            root->data = succ->data;
            root->right = deleteNode(root->right, succ->data);
        }
        return root;
    }
}