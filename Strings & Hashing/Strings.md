## **STRINGS**

Tips =
- Throw hashmap/sliding window/two ptrs on string problem if can't go forward
- stacks & queues go well with strings especially expressions
- whenever input says only lower case then think of using vector<int>(26) for hashing
- unique freq map for each string
```
for (int i = 0; i < N; ++i) {
    for (char c : words[i])
        freqMap[i][c - 'a']++;
    string key = "";
    for (int c = 0; c < 26; ++c)
        key += to_string(freqMap[i][c]);
    keyMap[key].push_back(i + 1);
}
```

Problems =

- Find the leftmost character which is repeating
- Find the leftmost character which is not repeating

- Find lexicographic rank of a string in O(N)
- (**IMP**)Find next permutation of a string

- (**IMP**)First Non-Repeating Char in a Stream ->
1. Print for the entire string the first non-repeating char (https://leetcode.com/problems/first-unique-character-in-a-string/)
    1. 2 Strings Traversals + HashMap (https://www.geeksforgeeks.org/given-a-string-find-its-first-non-repeating-character/)
    2. 1 String Traversal + 1 HashMap Traversal (https://www.geeksforgeeks.org/given-a-string-find-its-first-non-repeating-character/)
2. Print for every char, till now what is the first non-repeating char (https://www.interviewbit.com/problems/first-non-repeating-character-in-a-stream-of-characters/)
    Basically here we need to do O(1) for each query.
    So adding character, returning the first non-repeating character, deleting the repeating character all should be O(1)
    DLL + HashMap can do this. ALl non-repeating chars are stored in the order they appear in DLL. If we encounter repeating char(we can just use hashmap to see if that char is already present in DLL) then delete it using previous ref. of that node. 
    HashMap :- character -> node
    Deletion :- We use DLL to get prev. ref of that node.
    1. Queue + HashMap (https://www.geeksforgeeks.org/queue-based-approach-for-first-non-repeating-character-in-a-stream/)
    2. DLL + HashMap (https://www.geeksforgeeks.org/find-first-non-repeating-character-stream-characters/, https://www.youtube.com/watch?v=1kkvhg8S4O0&t=978s)

- https://www.geeksforgeeks.org/determine-string-unique-characters/
Constant Space On Strings = Then use hashmap
Constant Space + No Extra DS = Think of bits

- https://www.interviewbit.com/problems/longest-common-prefix/?ref=random-problem