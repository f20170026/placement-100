#include <bits/stdc++.h>
using namespace std;

int leftMost(string str) {
    int n = str.length();
    int firstIndex[256];
    memset(firstIndex, -1, sizeof(firstIndex));
    int res = INT_MAX;
    for (int i = 0; i < n; ++i) {
        char c = str[i];
        //first occurence of c
        if (firstIndex[c] == -1)
            firstIndex[c] = i;
        //c is repeating so wont be the answer
        else
            firstIndex[c] = -2;
    }
    for (int i = 0; i < 256; ++i)
        if (firstIndex[i] >= 0)
            res = min(res, firstIndex[i]);
    return (res == INT_MAX) ? -1 : res;
}

int main() {
    cout << leftMost("geeksforgeeks") << endl;  //5
    cout << leftMost("hello") << endl;          //0
}