#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/count-maximum-points-on-same-line/
 * Time O(N^2 logN) | Space O(N^2)
 */

int maxPoints(vector<int> &X, vector<int> &Y) {
    /**
     * Instead of using y2-y1/x2-x1 which might lead to division by zero, precision errors etc.
     * we rep. slope as {y2-y1, x2-x1} but the diff. should be in least fraction form ,so divide by their gcd so that they are in least fraction
     */
    map<pair<int, int>, int> slopeCount;  //{y2-y1,x2-x1} -> count
    int N = X.size();
    int maxPoints = 0;
    for (int i = 0; i < N; ++i) {
        int currMaxPoints = 0;
        slopeCount.clear();
        int samePoint = 0;
        for (int j = i + 1; j < N; ++j) {
            int yDiff = Y[j] - Y[i];
            int xDiff = X[j] - X[i];
            //If i & j are same point then gcd will be 0 and we can't divide by 0 so consider it separately
            if (xDiff == 0 && yDiff == 0) {
                samePoint++;
                continue;
            }
            int g = __gcd(xDiff, yDiff);
            xDiff /= g;
            yDiff /= g;
            slopeCount[{yDiff, xDiff}]++;
            currMaxPoints = max(currMaxPoints, slopeCount[{yDiff, xDiff};])
        }
        maxPoints = max(maxPoints, currMaxPoints + 1 + samePoint);
    }
    return maxPoints;
}
