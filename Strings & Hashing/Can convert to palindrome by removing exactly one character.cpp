#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/convert-to-palindrome/

bool isPalindrome(string s, int low, int high) {
    while (low < high) {
        if (s[low] != s[high]) return false;
        low++;
        high--;
    }
    return true;
}

int solve(string str) {
    int n = str.length();
    int low = 0, high = n - 1;
    while (low < high) {
        if (str[low] == str[high]) {
            low++;
            high--;
        } else {
            //Remove str[low]
            if (isPalindrome(str, low + 1, high)) return true;

            //Remove str[high]
            if (isPalindrome(str, low, high - 1)) return true;

            return false;
        }
    }
    return true;
}