#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/job-sequencing/

//O(N^2) Time | O(N) Space
class QuadraticTime {
   public:
    struct Job {
        int deadline;
        int profit;
        Job(int d, int p) : deadline(d), profit(p) {}
    };

    int maxDeadline;
    bool comp(Job a, Job b) {
        //true if a should come before b
        maxDeadline = max({maxDeadline, a.deadline, b.deadline});
        return a.profit >= b.profit;
    }

    int solve(vector<int> &deadline, vector<int> &profit) {
        int N = deadline.size();
        vector<Job> jobs;
        for (int i = 0; i < N; ++i) {
            jobs.push_back(Job(deadline[i], profit[i]));
        }
        maxDeadline = INT_MIN;
        sort(jobs.begin(), jobs.end(), comp);
        vector<bool> slots(maxDeadline - 1, false);  //slot[i] = true means we allotted a job in (i, i + 1) frame

        int maxProfit = 0;
        for (Job job : jobs) {
            //Try to allot this job as last as possible so as to accomadate other jobs in the front
            for (int i = job.deadline - 1; i >= 0; --i) {
                if (slots[i] == false) {
                    slots[i] = true;  //assign this job in that slot
                    maxProfit += job.profit;
                    break;
                }
            }
        }
        return maxProfit;
    }
};

/**
 * Optimization
 * for (Job job : jobs) {
 *      This loop for finding free slot is the bottleneck
        for (int i = job.deadline - 1; i >= 0; --i) {
            if (slots[i] == false) {
                slots[i] = true;  //assign this job in that slot
                maxProfit += job.profit;
                break;
            }
        }
    }
 * We will use DSU to optimize it
 * https://www.geeksforgeeks.org/job-sequencing-using-disjoint-set-union/
 */
struct Job {
    int deadline;
    int profit;
    Job(int d, int p) : deadline(d), profit(p) {}
};

int maxDeadline;
bool comp(Job a, Job b) {
    //true if a should come before b
    maxDeadline = max({maxDeadline, a.deadline, b.deadline});
    return a.profit >= b.profit;
}

class DisjointSet {
   private:
    vector<int> parent;

   public:
    DisjointSet(int size) {
        parent.resize(size + 1);
        for (int i = 0; i <= size; ++i)
            parent[i] = i;
    }

    //Finds free slot v <= u
    //0 is dummy node so if find(u) returns 0 then means no free slot
    int find(int u) {
        //if u is the root
        if (u == parent[u])
            return u;

        //path by compression
        return parent[u] = find(parent[u]);
    }

    //Slot v has become allotted. So u is the previous slot so whenever another
    //job needs slot before v we will return representative of u.
    void _union(int u, int v) {
        parent[v] = find(u);
    }
};

int solve(vector<int> &deadline, vector<int> &profit) {
    int N = deadline.size();
    vector<Job> jobs;
    for (int i = 0; i < N; ++i) {
        jobs.push_back(Job(deadline[i], profit[i]));
    }
    maxDeadline = INT_MIN;
    sort(jobs.begin(), jobs.end(), comp);

    DisjointSet dsu(maxDeadline);

    int maxProfit = 0;
    for (Job job : jobs) {
        //for deadline we are considering [deadline, deadline+1] in dsu as the interval
        int freeSlot = dsu.find(job.deadline);
        if (freeSlot > 0) {
            dsu._union(dsu.find(freeSlot - 1), freeSlot);
            maxProfit += job.profit;
        }
    }
    return maxProfit;
}
