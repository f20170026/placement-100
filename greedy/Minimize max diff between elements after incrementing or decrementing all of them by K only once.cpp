#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/minimize-the-difference-between-minimum-and-maximum-elements/
 * Given an array of N integers and an integer k. It is allowed to modify an element either by increasing or decreasing them by k (only once).
 * The task is to minimize and print the maximum difference between the shortest and longest towers.
 */
int minimizeDiff(int* arr, int n, int k) {
    // Find max and min elements of the array
    int max = *(max_element(arr, arr + n));
    int min = *(min_element(arr, arr + n));

    // Check whether the difference between
    // the max and min element is less than
    // or equal to k or not
    if ((max - min) <= k) {
        return (max - min);
    }

    // Calculate average of max and min
    int avg = (max + min) / 2;

    for (int i = 0; i < n; i++) {
        // If the array element is greater than the
        // average then decrease it by k
        if (arr[i] > avg)
            arr[i] -= k;
        // If the array element is smaller than the
        // average then increase it by k
        else
            arr[i] += k;
    }

    // Find max and min of the modified array
    max = *(max_element(arr, arr + n));
    min = *(min_element(arr, arr + n));

    // return the new difference
    return (max - min);
}