#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/jump-game-ii/
 * Best explanation = https://leetcode.com/problems/jump-game-ii/discuss/696926/C%2B%2B-Intuitive-Greedy-with-Explanation-O(n)
 * First approach that comes to mind is the O(n^2) DP approach. A better approach =>
 * To design a greedy algorithm we can use the following observation -
 * Say we want to reach the index number i in minimum jumps. 
 * Let j denote all the candidates for the previous index (all indices such that j+nums[j]>=i and j<i). 
 * Then, for the optimal path, we will choose the j that is the farthest left from i. 
 * The reason for this is that if we dont choose the farthest left( but instead choose some later j) 
 * then we can always create a better or equivalent solution by replacing this value with the farthest value. 
 * This is our greedy choice.
 * So we will pre-compute for all indexes the leftMostIndex which can reach this index in one step
 */

int jump(vector<int>& nums) {
    int n = nums.size();
    vector<int> leftMostIndex(n);
    leftMostIndex[0] = -1;

    //O(N) Time
    int l = 0;
    for (int r = 1; r < n; ++r) {
        if (l + nums[l] >= r) {
            leftMostIndex[r] = l;
        } else {
            while (l + nums[l] < r) {
                l++;
            }
            leftMostIndex[r] = l;
        }
    }

    //O(N) Time
    int currentIndex = n - 1;
    int minJumps = 0;
    while (currentIndex != 0) {
        //leftMostIndex[currInd] ---> currInd (taking this jump)
        minJumps++;
        currentIndex = leftMostIndex[currentIndex];
    }
    return minJumps;
}
