#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.interviewbit.com/problems/gas-station/
 * Brute force is to start from i and maintain total gas present and if at some j we get get tank < gas needed from j to j+1 we stop and and do same for i + 1 and so on.
 * If we start from i and at j it is not possible to go to j + 1 then we can skip i+1,i+2....,j (think about this)
 * That's why change starting point to 
 */

int canCompleteCircuit(const vector<int> &gas, const vector<int> &need) {
    int n = gas.size();
    int totalGas = 0, totalNeed = 0;
    for (int i = 0; i < n; ++i) {
        totalGas += gas[i];
        totalNeed += need[i];
    }
    if (totalNeed > totalGas) return -1;

    int tank = 0;
    int startingPoint = 0;
    for (int i = 0; i < n; ++i) {
        //Can we move from i to i+1? need[i] = gas needed to go from i to i + 1
        if (tank + gas[i] >= need[i]) {
            tank += gas[i] - need[i];
        } else {
            //then startingPoint needs to be changed to i + 1 since we can't start from [startPoint+1,startingPoint+2,....i]
            startingPoint = i + 1;
            tank = 0;
        }
    }
    return startingPoint;
}
