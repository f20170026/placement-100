#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/segregate-0s-and-1s-in-an-array-by-traversing-array-once/
 * Segregate 0s and 1s in an array (Sort Binary Array)
 * 1. Using two pointers one at beginning, one at end
 * 2. One pointer to oneHead and another iterator [Maintains ordering of 1st half elements. 2nd half elemnets may not be in same order]
 */

void sortBinaryArray1(vector<int> &arr) {
    int left = 0, right = arr.size() - 1;
    while (left < right) {
        //arr[left] = 1 & arr[right] = 0 is mismatch so need to swap
        //find that pair
        while (left <= right && arr[left] == 0)
            left++;
        while (left <= right && arr[right] == 1)
            right--;
        if (left < right) {
            arr[left++] = 0;
            arr[right--] = 1;
        }
    }
    printArray(arr);
}

void sortBinaryArray2(vector<int> &arr) {
    if (arr.empty() || arr.size() == 1) {
        printArray(arr);
        return;
    }

    int zeroHead;  //points to first 1
    for (zeroHead = 0; zeroHead < arr.size(); ++zeroHead) {
        if (arr[zeroHead] == 1) break;
    }
    //arr[0....zeroHead - 1] are 0s for sure
    for (int i = zeroHead + 1; i < arr.size(); ++i) {
        if (arr[i] == 0) {
            int temp = arr[zeroHead];
            arr[zeroHead] = arr[i];
            arr[i] = temp;
            zeroHead++;
            while (zeroHead < arr.size() && arr[zeroHead] == 0)
                zeroHead++;
        }
    }
    printArray(arr);
}

/**
 * https://leetcode.com/problems/move-zeroes/
 * Move Zeroes To The end while maintaing relative order of non-zero elements
 * Use zeroHead pointer
 */

void moveZeroes(vector<int> &nums) {
    int n = nums.size();
    if (n == 0 || n == 1) return;

    int zeroHead;
    for (zeroHead = 0; zeroHead < n; ++zeroHead)
        if (nums[zeroHead] == 0) break;

    for (int i = zeroHead + 1; i < n; ++i) {
        if (nums[i] != 0) {
            swap(nums[i], nums[zeroHead++]);
        }
    }
}

/**
 * https://www.geeksforgeeks.org/segregate-even-and-odd-numbers/
 * Segregate Even and Odd numbers
 * Using oddHead pointer
 */
void segregrateEvenOdd(vector<int> &arr) {
    int n = arr.size();
    int oddHead;
    for (oddHead = 0; oddHead < n; ++oddHead)
        if (arr[oddHead] & 1)
            break;

    //arr[0.....oddHead - 1] are even so don't bother about them
    for (int i = oddHead + 1; i < n; ++i) {
        if (arr[i] % 2 == 0) {
            //even
            swap(arr[i], arr[oddHead++]);
        }
    }
    printArray(arr);
}

/**
 * https://leetcode.com/problems/sort-colors
 * Segregrate 0s, 1s, 2s (DUTCH NATIONAL FLAG PROBLEM)
 * @throw Invalid input exception
 */
void sortColors(vector<int> &nums) {
    int n = nums.size();
    if (n == 0 || n == 1) return;

    int low = 0, mid = 0, high = n - 1;
    //low points to index after zero head
    //high points to index before two head
    //mid is the pointer traversing throughout the array to swap mis-matches

    while (low <= mid && mid <= high) {
        int curr_ele = nums[mid];
        if (curr_ele == 1) {
            mid++;
        } else if (curr_ele == 0) {
            swap(nums[low++], nums[mid++]);
        } else if (curr_ele == 2) {
            //Dont increase mid in this case since the nums[high] may be 0 or 1
            //If it is 1 then we will increase anyway but if it is 0 then need to swap
            swap(nums[mid], nums[high--]);
        } else {
            throw "Invalid input. Should contain only 0, 1, 2";
        }
    }
}

void printArray(vector<int> &arr) {
    for (int num : arr) cout << num << " ";
    cout << endl;
}
