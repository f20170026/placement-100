#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/spiral-matrix-ii/description/
 * Generating incremental matrix in spiral order
 * Given a positive integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.
 * Input: 3
 * Output:[
 * [ 1, 2, 3 ],
 * [ 8, 9, 4 ],
 * [ 7, 6, 5 ]]
 */
class Solution {
   public:
    vector<vector<int>> generateMatrix(int N) {
        vector<vector<int>> matrix(N, vector<int>(N, -1));
        int rS = 0, rE = N - 1, cS = 0, cE = N - 1;
        int number = 1;
        /**
         * 0. Left to right
         * 1. top to bottom
         * 2. right to left
         * 3. bottom to top
         */
        int direction = 0;
        while (number <= N * N) {
            if (direction == 0) {
                for (int col = cS; col <= cE; ++col)
                    matrix[rS][col] = number++;
                rS++;
            } else if (direction == 1) {
                for (int row = rS; row <= rE; ++row)
                    matrix[row][cE] = number++;
                cE--;
            } else if (direction == 2) {
                for (int col = cE; col >= cS; --col)
                    matrix[rE][col] = number++;
                rE--;
            } else if (direction == 3) {
                for (int row = rE; row >= rS; --row)
                    matrix[row][cS] = number++;
                cS++;
            }
            direction = (direction + 1) % 4;
        }
        return matrix;
    }
};