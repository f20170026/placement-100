#include <bits/stdc++.h>
using namespace std;

int maxSubArray(vector<int> a) {
    if (a.empty())
        return 0;

    int max_so_far = a[0];
    int curr_max = a[0];

    for (int i = 1; i < a.size(); i++) {
        curr_max = max(a[i], curr_max + a[i]);
        max_so_far = max(max_so_far, curr_max);
    }
    return max_so_far;
}

//Do maxSumSubarray on negative of this array
int minSubArray(vector<int> a) {
    vector<int> negative_a;
    for (int num : a)
        negative_a.push_back(-1 * num);

    return -1 * maxSubArray(negative_a);
}

/**
 * https://www.geeksforgeeks.org/maximum-contiguous-circular-sum/
 * 1. Use extra space. Repeat the same array after the end of the array. Find max sum subarray in this 2 * N array of max window size N
 * 2. maximum of (normal max subarry sum) and (total sum - minimum subarray sum). 1st case if for no wrapping. 2nd case when there is wrapping
 */
int maxCircularSum(vector<int> a) {
    int noWrappingMaxSum = maxSubArray(a);

    int totalSum = 0;
    for (int num : a)
        totalSum += num;

    //[.....XXXX.....] thoses Xs form min sum. remove them from overall sum
    int wrappingSum = totalSum - minSubArray(a);
    if (wrappingSum == 0) {
        //Don't consider empty subarray case
        return noWrappingMaxSum;
    }
    return max(wrappingSum, noWrappingMaxSum);
}

int maxSubMatrix(vector<vector<int>> a) {
    int rows = a.size();
    int cols = a[0].size();
    int ans = INT_MIN;
    for (int leftCol = 0; leftCol < cols; ++leftCol) {
        vector<int> rowSum(rows, 0);
        for (int rightCol = leftCol; rightCol < cols; ++rightCol) {
            //Calculate Current Row Sum Bounded by leftCol --- rightCol
            for (int i = 0; i < rows; ++i) {
                rowSum[i] += a[i][rightCol];
            }

            //Now Do Kadane's Algo On This Array
            ans = max(ans, maxSubArray(rowSum));
        }
    }
    return ans;
}