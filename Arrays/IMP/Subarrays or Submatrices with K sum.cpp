#include <bits/stdc++.h>
using namespace std;

/**
 * Prints the Largest length subarray with sum K (general version)
 * O(N) Time | O(N) Space
 * Variation = https://www.geeksforgeeks.org/longest-span-sum-two-binary-arrays/ 
 * Subtract two arrays and find largest subarray with zero sum
 * Why? since a1[i]+a1[i+1]+....+a1[j] = (j-i+1)/2 = a2[i]+a2[i+1]+...+a2[j] => (a1[i] - a2[i]) + (a1[i+1] - a2[i+1]) +...+ (a1[j] - a2[j]) = 0.
 */
vector<int> largestLengthSubarray(vector<int> arr, int K) {
    long long sum = 0;
    int left = 0, right = -1;
    unordered_map<int, int> sumIndex;  //subarray sum -> ending index of subarray
    for (int i = 0; i < arr.size(); ++i) {
        sum += arr[i];
        if (sum == K) {
            int size = i + 1;
            if (size > right - left + 1) {
                left = 0;
                right = i;
            }
        } else {
            if (sumIndex.find(sum - K) == sumIndex.end()) {
                sumIndex[sum] = i;
            } else {
                //[l+1 ..... r] has sum as K
                int l = sumIndex[sum - K] + 1;
                int r = i;
                if (r - l + 1 > right - left + 1) {
                    left = l;
                    right = r;
                }
            }
        }
    }
    vector<int> result;
    for (int i = left; i <= right; ++i)
        result.push_back(arr[i]);
    return result;
}
/**
 * Number of subarrays with sum K (general version)
 * O(N) Time | O(N) Space
 * Variation = Given a binary array count number of subarrays with equal no. of 0s and 1s in it.
 * Replace 0 with -1 and find number of subarrays with sum as 0. It is the answer. Can be extended to binary matrix also
 */
int countSubarrayWithSumK(vector<int> arr, int K) {
    long long sum = 0;
    unordered_map<int, int> sumCount;  //subarray sum -> count of how many times it has occured
    int result = 0;
    for (int i = 0; i < arr.size(); ++i) {
        sum += arr[i];
        if (sum == K) {
            result++;
        }

        if (sumCount.find(sum - K) != sumCount.end()) {
            //[l+1 ..... r] has sum as K
            result += sumCount[sum - K];
        }

        sumCount[sum]++;
    }
    return result;
}
/**
 * Count submatrices with sum K
 * O(col*col*row) Time | O(row) Space
 * So if rows are larger than cols then use leftRow & rightRow bounds and calculate col-sum.
 * You can also find largest area with sum K
 * Variation = https://www.geeksforgeeks.org/largest-area-rectangular-sub-matrix-equal-number-1s-0s/ (Replacing 0 with -1 and finding largest area)
 */
int countSubmatrixWithSumK(vector<vector<int>> matrix, int K) {
    int rows = matrix.size();
    int cols = matrix[0].size();
    int result = 0;
    for (int leftCol = 0; leftCol < cols; ++leftCol) {
        vector<int> rowSum(rows, 0);
        for (int rightCol = leftCol; rightCol < cols; ++rightCol) {
            //Calc rowSum
            for (int row = 0; row < rows; ++row)
                rowSum[row] += matrix[row][rightCol];

            //Find number of subarrays in rowSum with sum as K
            result += countSubarrayWithSumK(rowSum, K);
        }
    }
    return result;
}

int main() {
    cout << countSubarrayWithSumK({10, 2, -2, -20, 10}, -10) << endl;  //3
    cout << countSubarrayWithSumK({9, 4, 20, 3, 10, 5}, 33) << endl;   //2
}