#include <bits/stdc++.h>
#define deb(x) cout << #x << " " << x << endl;
using namespace std;

/**
 * https://www.spoj.com/submit/PALIN
 * Many cases
 * 1. Even length String
 *       a) rev(1st half) <= 2nd half then str[mid1]++ (propagate carry if any towards left) and then copy the rev(1st half) into 2nd half
 *       b) rev(1st half) > 2nd half then directly copy the rev(1st half) into 2nd half
 * 2. Odd length String
 *       a) rev(1st half) <= 2nd half then str[mid]++ here mid is n/2 (propagate carry if any towards left) and then copy the rev(1st half) into 2nd half
 *       b) rev(1st half) > 2nd half then directly copy the rev(1st half) into 2nd half
 * Variations = https://www.geeksforgeeks.org/closest-palindrome-number-whose-absolute-difference-min/
 */
void generateNextPalindromeUtil(string &str, int n) {
    int mid1, mid2;
    if (n & 1) {
        mid1 = n / 2 - 1;
        mid2 = n / 2 + 1;
    } else {
        mid1 = n / 2 - 1;
        mid2 = n / 2;
    }
    string left = str.substr(0, mid1 + 1);
    string right = str.substr(mid2);
    string revLeft = reverseString(left);
    if (n % 2 == 0) {
        if (revLeft <= right) {
            while (left[mid1] == '9') {
                left[mid1] = '0';
                str[mid1] = '0';
                mid1--;
            }
            left[mid1]++;
            str[mid1]++;
            revLeft = reverseString(left);
            stringCopy(revLeft, str, mid2);
        } else {
            stringCopy(revLeft, str, mid2);
        }
    } else {
        int mid = n / 2;
        if (revLeft <= right) {
            while (str[mid] == '9') {
                str[mid--] = '0';
            }
            str[mid]++;
            left = str.substr(0, mid1 + 1);
            revLeft = reverseString(left);
            stringCopy(revLeft, str, mid2);
        } else {
            stringCopy(revLeft, str, mid2);
        }
    }
}

int all_nine(string str)  //check if all digits are '9'
{
    for (int i = 0; i < str.length(); i++) {
        if (str[i] != '9')
            return 0;
    }
    return 1;
}

//Place revLeft in str from index 'ind'
void stringCopy(string &revLeft, string &str, int ind) {
    int k = 0;
    for (int i = ind; i < str.length(); ++i) {
        str[i] = revLeft[k++];
    }
}

string reverseString(string &str) {
    string rev = str;
    reverse(rev.begin(), rev.end());
    return rev;
}

int main() {
    int t;
    cin >> t;

    while (t--) {
        string str;
        cin >> str;
        int i, j, num;
        char ans[str.length() + 3];
        int n = str.length();

        if (all_nine(str)) {  //if all 9
            ans[0] = '1';
            for (i = 0; i < n; i++) {
                ans[i + 1] = '0';
            }
            ans[n] = '1';
            ans[n + 1] = '\0';
            cout << ans << endl;
        }

        else {
            generateNextPalindromeUtil(str, str.length());
            cout << str << endl;
        }
    }
    return 0;
}