#include <bits/stdc++.h>
using namespace std;
void print(vector<vector<int>> arr) {
    for (vector<int> vec : arr) {
        for (int num : vec) cout << num << " ";
        cout << endl;
    }
    cout << endl;
}
/**
 * 1. All unique pairs in sorted array which add up to given sum         (2 Sum)
 * 2. All unique pairs in unsorted array which add up to given sum       (2 Sum)
 * 3. All unique triplets in sorted array which add up to given sum      (3 Sum)
 * 4. All unique triplets in unsorted array which add up to given sum    (3 Sum)
 * 5. All unique quadruplets in sorted array which add up to given sum   (4 sum)
 * 6. All unique quadruplets in unsorted array which add up to given sum (4 sum)
 * 
 * 7. Two Sum in two arrays
 * 8. Find A[i] - A[j] == K (i != j)
 * 9. Find A[i] - B[j] == K (Two arrays)
 */

//2-sum sorted. O(N) Time | O(1) Space
vector<vector<int>> twoNumberSumSorted(vector<int> arr, int targetSum) {
    int n = arr.size();
    int i = 0;
    int j = n - 1;
    vector<vector<int>> allPairs;

    while (i < j) {
        int sum = arr[i] + arr[j];
        if (sum < targetSum)
            ++i;
        else if (sum > targetSum)
            --j;
        else {
            //Found a pair
            allPairs.push_back({arr[i], arr[j]});
            //To avoid duplicates
            ++i;
            while (i < j && arr[i - 1] == arr[i])
                ++i;
            --j;
            while (i < j && arr[j] == arr[j + 1])
                --j;
        }
    }

    print(allPairs);
    return allPairs;
}

//2-sum unsorted. You can sort the array and use the above method. O(NlogN) Time | O(1) Space
//Use hashing. O(N) Time | O(N) space
vector<vector<int>> twoNumberSumUnSorted(vector<int> arr, int targetSum) {
    vector<vector<int>> allPairs;
    unordered_set<int> hasSeen;

    hasSeen.insert(arr[0]);
    int i = 1;
    int N = arr.size();
    while (i < N) {
        int target = targetSum - arr[i];
        if (hasSeen.find(target) != hasSeen.end()) {
            allPairs.push_back({target, arr[i]});
            //Don't need to insert arr[i] again since we need unique pairs only but skip the duplicates
            ++i;
            while (i < N && arr[i - 1] == arr[i])
                ++i;
        } else {
            hasSeen.insert(arr[i]);
            ++i;
        }
    }

    print(allPairs);
    return allPairs;
}

vector<vector<int>> threeNumberSumSorted(vector<int> array, int targetSum, int ind = 0) {
    sort(array.begin(), array.end());
    vector<vector<int>> result;
    //If sorted array then this method. O(N^2) Time | O(1) Space
    for (int i = ind; i < array.size();) {
        //Search from i+1 till end for doublet whose sum is targetSum - array[i]
        int lo = i + 1;
        int hi = array.size() - 1;
        while (lo < hi) {
            int sum = array[lo] + array[hi];
            if (sum < (targetSum - array[i]))
                lo++;
            else if (sum > (targetSum - array[i]))
                hi--;
            else {
                result.push_back({array[i], array[lo], array[hi]});
                //To avoid duplicates
                ++lo;
                while (lo < hi && array[lo - 1] == array[lo])
                    ++lo;
                --hi;
                while (lo < hi && array[hi] == array[hi + 1])
                    --hi;
            }
        }
        //Skip duplicates
        ++i;
        while (i < array.size() && array[i] == array[i - 1])
            ++i;
    }
    return result;
}
vector<vector<int>> threeNumberSumUnSorted(vector<int> array, int targetSum, int ind = 0) {
    //If unsorted array then this method. Not recommended. Since same time as sorted version but extra linear space
    int N = array.size();
    vector<vector<int>> result;
    unordered_set<int> hasSeen;
    for (int i = 0; i < N;) {
        int j = i + 1;
        while (j < N) {
            int sum = array[i] + array[j];
            int target = targetSum - sum;
            if (hasSeen.find(target) != hasSeen.end()) {
                result.push_back({target, array[i], array[j]});
                ++j;
                //To avoid duplicates
                while (j < N && array[j - 1] == array[j])
                    ++j;
            } else {
                ++j;
            }
        }
        hasSeen.insert(array[i]);
        //Skip duplicates
        ++i;
        while (i < array.size() && array[i] == array[i - 1])
            ++i;
    }

    return result;
}

vector<vector<int>> fourNumberSumSorted(vector<int> array, int targetSum) {
    sort(array.begin(), array.end());
    vector<vector<int>> result;
    //If sorted array use this method. O(N^3) Time | O(1) Space
    for (int i = 0; i < array.size();) {
        //O(N^2) for threeeNumberSum
        vector<vector<int>> vec = threeNumberSumSorted(array, targetSum - array[i], i + 1);
        if (vec.size() > 0) {
            for (vector<int> temp : vec) {
                temp.push_back(array[i]);
                result.push_back(temp);
            }
        }
        //Skip duplicates
        ++i;
        while (i < array.size() && array[i - 1] == array[i])
            ++i;
    }
    for (vector<int> &vec : result)
        sort(vec.begin(), vec.end());
    sort(result.begin(), result.end());
    return result;
}

//Doesn't work when there are duplicate numbers in the array.
vector<vector<int>> fourNumberSumUnsorted(vector<int> array, int targetSum) {
    vector<vector<int>> result;
    int N = array.size();
    unordered_map<int, vector<vector<int>>> seenPairs;
    for (int i = 0; i < N; ++i) {
        for (int j = i + 1; j < N; ++j) {
            int sum = array[i] + array[j];
            int target = targetSum - sum;
            if (seenPairs.find(target) != seenPairs.end()) {
                vector<vector<int>> pairs = seenPairs[target];
                for (vector<int> pair : pairs) {
                    pair.push_back(array[i]);
                    pair.push_back(array[j]);
                    result.push_back(pair);
                }
            }
        }

        for (int k = 0; k < i; ++k) {
            int sum = array[i] + array[k];
            seenPairs[sum].push_back({array[i], array[k]});
        }
    }
    for (vector<int> &vec : result)
        sort(vec.begin(), vec.end());
    sort(result.begin(), result.end());
    return result;
}

void twoSumTwoArrays(vector<int> A, vector<int> B, int targetSum) {
    //Sorted. O(N + M) Time | O(1) Space
    //Variation => https://www.interviewbit.com/problems/find-the-closest-pair-from-two-sorted-arrays/ (Before the if conditions just update the minimum)
    int lo = 0, hi = B.size() - 1;
    while (lo < A.size() && hi >= 0) {
        int currentSum = A[lo] + B[hi];
        if (currentSum == targetSum) {
            cout << A[lo] << " " << B[hi] << endl;
            lo++;
            hi--;
        } else if (currentSum < targetSum) {
            lo++;
        } else {
            hi--;
        }
    }

    //Unsorted (hashing) O(min(N, M)) Time | O(max(N, M)) Space
    //Hash longer array
    unordered_set<int> seen;
    for (int num : A)
        seen.insert(targetSum - num);

    for (int i = 0; i < A.size(); ++i) {
        if (seen.find(B[i]) != seen.end()) {
            cout << targetSum - B[i] << " " << B[i] << endl;
        }
        //Skip duplicates
        ++i;
        while (i < A.size() && A[i] == A[i - 1])
            ++i;
    }
}

//Variation = two pair difference in two arrays. Use two pointers (firstPtr = 0 in one array, secondPtr = 0 in other array)
void pairDifferenceInOneArray(vector<int> arr, int n, int targetDiff) {
    sort(arr.begin(), arr.end());
    //Sorted array => O(N) Time | O(1) Space
    int lo = 0, hi = 1;
    int diff = abs(targetDiff);
    int count = 0;
    while (hi < n) {
        int currDiff = arr[hi] - arr[lo];
        if (lo != hi && currDiff == targetDiff) {
            count++;
            lo++;
            hi++;
        } else if (currDiff < targetDiff || lo == hi) {
            hi++;
        } else {
            lo++;
        }
    }

    //Unsorted array => O(N) Time | O(N) space
    unordered_set<int> seen;
    int count = 0;
    for (int num : arr) {
        if (seen.find(num) != seen.end()) {
            count++;
        }
        seen.insert(num + targetDiff);
        seen.insert(num - targetDiff);
    }
}

int main() {
    // twoNumberSumSorted({1, 1, 2, 45, 46, 46}, 47);
    // twoNumberSumSorted({1, 1}, 2);
    // twoNumberSumSorted({1, 1, 5, 5}, 6);

    // threeNumberSum1{12, 3, 1, 2, -6, 5, -8, 6}, 0);
}