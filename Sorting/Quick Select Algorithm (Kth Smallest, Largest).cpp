#include <bits/stdc++.h>
using namespace std;

/**
 * Kth Smallest => Element at (K - 1)th index in sorted array.
 * So use quick sort type except here after partitioning we recur only one half.
 * 1. Select Random pivot index
 * 2. Partition the array around that pivot
 * 3. If that pivot final position is (K - 1) then it is the answer else recur for one of the half
 * Average : T(N) = T(N/2) + O(N). This depends on choice of pivot. O(N) on average but in worst case (bad pivot) T(N) = T(N - 1) + O(N) => O(N^2)
 * Kth Largest => Element at (N - K)th index in sorted array which (N - K + 1)th smallest element.
 * Printing K smaller elements, K large elements can be done after calling this function
 * Application => Can find median using this (median is n/2th smallest guy)
 */
class Solution {
   private:
    void swap(vector<int> &arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    int partition(vector<int> &arr, int left, int right, int pivotIndex) {
        //Swap pivot element to the last element.
        swap(arr, right, pivotIndex);

        //Iterate in [left....right-1] and partition
        int pivotElement = arr[right];
        int lessTailIndex = left;  //will point to tail of lesser elements of pivot [left.....lessTailIndex-1] are elements less than pivot

        for (int i = left; i < right; ++i) {
            if (arr[i] < pivotElement) {
                swap(arr, i, lessTailIndex);
                lessTailIndex++;
            }
        }

        //Swap pivot element to its correct place
        swap(arr, lessTailIndex, right);
        return lessTailIndex;
    }

    //Finds Kth Smallest Element
    //if finding kth largest element then it is (n-k+1)th smallest element
    int quickSelect(vector<int> &arr, int K) {
        if (K > arr.size()) {
            throw "K should be smaller than size";
        }
        int left = 0, right = arr.size() - 1;
        //Keep on reducing search space till we find kth smallest element
        while (left <= right) {
            int size = right - left + 1;
            int pivotIndex = left + rand() % size;
            int finalPivotIndex = partition(arr, left, right, pivotIndex);
            if (finalPivotIndex == (K - 1)) {
                return arr[finalPivotIndex];
            }
            if (finalPivotIndex < (K - 1)) {
                left = finalPivotIndex + 1;
            } else {
                right = finalPivotIndex - 1;
            }
        }
    }

   public:
    //print last K elements
    void printKLargeElements(vector<int> arr, int K) {
        quickSelect(arr, arr.size() - K + 1);
        for (int i = arr.size() - K; i < arr.size(); ++i)
            cout << arr[i] << " ";
        cout << endl;
    }
    //[0.....K-1] are K small elements
    void printKSmallElements(vector<int> arr, int K) {
        quickSelect(arr, K);
        for (int i = 0; i < K; ++i)
            cout << arr[i] << " ";
        cout << endl;
    }
    int KthSmallest(vector<int> arr, int K) {
        return quickSelect(arr, K);
    }
    int KthLargest(vector<int> arr, int K) {
        return quickSelect(arr, arr.size() - K + 1);
    }
};

int main() {
    try {
        Solution s;
        auto vec = {2, 3, 5, 1, 4};
        cout << s.KthSmallest(vec, 1) << endl;  //1
        cout << s.KthSmallest(vec, 2) << endl;  //2
        cout << s.KthSmallest(vec, 3) << endl;  //3
        cout << s.KthSmallest(vec, 4) << endl;  //4
        cout << s.KthSmallest(vec, 5) << endl;  //5

        cout << s.KthLargest(vec, 1) << endl;  //5
        cout << s.KthLargest(vec, 2) << endl;  //4
        cout << s.KthLargest(vec, 3) << endl;  //3
        cout << s.KthLargest(vec, 4) << endl;  //2
        cout << s.KthLargest(vec, 5) << endl;  //1

        s.printKSmallElements(vec, 4);
        s.printKLargeElements(vec, 2);
    } catch (const char *errorMessage) {
        cerr << errorMessage << endl;
    }
}