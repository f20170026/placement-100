#include <bits/stdc++.h>
using namespace std;

/**
 * https://www.geeksforgeeks.org/sort-array-according-order-defined-another-array/ (many methods)
 * 1. Custom sort => O(M + NlogN) Time | O(M) Space
 * 2. hashing freq of arr2 and then iterate through arr1 and place in output array based on freq from hash map. And remove from hash map
 *    And then sort other elements in the hashmap
 *    O(M + N + plogp) Time | O(M) space where p is number of elements left in arr1
 */

class Solution1 {
   public:
    unordered_map<int, int> indexMap;  //number->index map for arr2

    bool comp(int a, int b) {
        //return True if a should come before b
        bool a_present = (indexMap.find(a) != indexMap.end());
        bool b_present = (indexMap.find(b) != indexMap.end());

        if (!a_present && !b_present) {
            //both not present
            return a <= b;
        }
        if (a_present && b_present) {
            //both are present
            return indexMap[a] <= indexMap[b];
        }
        if (a_present) {
            //a is present but not b
            return true;
        } else {
            //a is not present but b is
            return false;
        }
    }

    vector<int> relativeSortArray(vector<int>& arr1, vector<int>& arr2) {
        indexMap.clear();
        for (int i = 0; i < arr2.size(); ++i)
            indexMap[arr2[i]] = i;
        sort(arr1.begin(), arr1.end(), comp);
        return arr1;
    }
};