#include <bits/stdc++.h>
using namespace std;

void merge(vector<int> &arr, int start, int mid, int end) {
    int size = end - start + 1;
    vector<int> mergedResult(size);
    int ptr1 = start, ptr2 = mid + 1;
    for (int i = 0; i < size; ++i) {
        int num;
        if (ptr1 > mid) {
            num = arr[ptr2++];
        } else if (ptr2 > end || arr[ptr1] < arr[ptr2]) {
            num = arr[ptr1++];
        } else {
            num = arr[ptr2++];
        }
        mergedResult[i] = num;
    }
    for (int num : mergedResult) {
        arr[start++] = num;
    }
}

//https://www.geeksforgeeks.org/counting-inversions/
//Two elements a[i] and a[j] form an inversion if a[i] > a[j] and i < j
void mergeSortRecursive(vector<int> &arr, int start, int end, int &count) {
    if (start < end) {
        int mid = start + (end - start) / 2;
        mergeSortRecursive(arr, start, mid, count);
        mergeSortRecursive(arr, mid + 1, end, count);
        /**
         * [start....mid] and [mid+1....end] are sorted so merge them
         * IMP = Count inequalities if you want now since sorted arrays.
         */
        int i = start, j = mid + 1;
        while (i <= mid && j <= end) {
            if (arr[i] > arr[j]) {
                count += mid - i + 1;
                j++;
            } else {
                i++;
            }
        }
        merge(arr, start, mid, end);
    }
}

//https://leetcode.com/problems/reverse-pairs/
//Given an array nums, we call (i, j) an important reverse pair if i < j and nums[i] > 2*nums[j].
void mergeSortRecursive(vector<int> &arr, int start, int end, int &count) {
    if (start < end) {
        int mid = start + (end - start) / 2;
        mergeSortRecursive(arr, start, mid, count);
        mergeSortRecursive(arr, mid + 1, end, count);
        //[start....mid] and [mid+1....end] are sorted so merge them
        //count inequalities
        int i = start;
        int j = mid + 1;
        while (i <= mid && j <= end) {
            if (1LL * arr[i] > 2LL * arr[j]) {
                count += (mid - i + 1);
                j++;
            } else {
                i++;
            }
        }
        merge(arr, start, mid, end);
    }
}

int mergeSort(vector<int> arr) {
    int count = 0;
    mergeSortRecursive(arr, 0, arr.size() - 1, count);
    return count;
}

class Solution {
   public:
    int reversePairs(vector<int> &nums) {
        return mergeSort(nums);
    }
};