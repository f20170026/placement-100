#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/find-k-closest-elements/
 * Find absolute difference of each element with the given and then use K smallest numbers logic
 * Time O(NlogK) | Space O(K)
 * There's a more efficient solution which can be done using binary search. 
 */

vector<int> KClosestNumbers(vector<int> arr, int K, int value) {
    if (K > arr.size()) {
        cout << "K should be less than number of elements\n";
        return {};
    }
    if (K == arr.size()) {
        //If we can return in any order the result
        return arr;
    }
    vector<int> result;                      //stores k smallest elements
    priority_queue<pair<int, int>> maxHeap;  // {absolute difference, original value}
    for (int i = 0; i < K; ++i) {
        maxHeap.push({abs(arr[i] - value), arr[i]});
    }

    for (int i = K; i < arr.size(); ++i) {
        if (abs(arr[i] - value) < maxHeap.top().first) {
            maxHeap.push({abs(arr[i] - value), arr[i]});
        }
    }

    while (!maxHeap.empty()) {
        result.push_back(maxHeap.top().second);
        maxHeap.pop();
    }
    reverse(result.begin(), result.end());
    return result;
}