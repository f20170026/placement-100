#include <bits/stdc++.h>
using namespace std;

//Stores element to frequency
unordered_map<int, int> FreqMap;
//Group all same frequency elements in one stack. This is to preserve the order and print in that order in case of tie in frequencies
unordered_map<int, stack<int>> StackMap;

//O(1) Time
void push(int data) {
    FreqMap[data]++;
    StackMap[FreqMap[data]].push(data);
    MaxFreq = max(MaxFreq, FreqMap[data]);
}

//O(N) Time. Remove Max Freq Element. In case of tie remove the element closest to the top.
int pop() {
    //In case of no elements
    if (FreqMap.size() == 0) {
        return -1;
    }
    int MaxFreq = INT_MIN;
    for (auto it : FreqMap) {
        MaxFreq = max(MaxFreq, it.second);
    }
    int MaxFreqEle = StackMap[MaxFreq].top();
    StackMap[MaxFreq].pop();
    FreqMap[MaxFreqEle]--;
    return MaxFreqEle;
}

/**
 * pop() takes O(N) time since we run a loop to find the maximum frequency.
 * We can avoid this by storing max frequency in a global variable and changing the frequency appropriately.
 * The new pop() takes O(1) time.
 */

int MaxFreq = 0;
int pop() {
    if (FreqMap.size() == 0) {
        return -1;
    }
    int MaxFreqEle = StackMap[MaxFreq].top();
    StackMap[MaxFreq].pop();
    FreqMap[MaxFreqEle]--;
    //This case is important. When there is no element with this maximum frequency then remove it.
    if (StackMap[MaxFreq].empty())
        MaxFreq--;
    return MaxFreqEle;
}