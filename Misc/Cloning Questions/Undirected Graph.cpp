#include <bits/stdc++.h>
using namespace std;

struct Node {
    int label;
    vector<Node*> neighbors;
    Node(int x) : label(x){};
};

// https://www.interviewbit.com/problems/clone-graph/?ref=random-problem

Node* cloneGraph(Node* node) {
    if (!node) {
        return NULL;
    }
    unordered_map<Node*, Node*> mappings;  //original node -> cloned node
    queue<Node*> q;
    q.push(node);
    Node* clonedNode = new Node(node->label);
    mappings[node] = clonedNode;

    while (!q.empty()) {
        Node* current = q.front();
        Node* clone = mappings[current];
        q.pop();

        for (Node* child : current->neighbors) {
            //current has child in its neighbors vector so we should have the
            //clone of this child in the clone of current's neighbors vector.
            //create a cloned node for the child if it doesn't exist
            if (mappings.find(child) == mappings.end()) {
                Node* clonedChild = new Node(child->label);
                mappings[child] = clonedChild;
                q.push(child);  //we only push if cloned child is not created. if it was already created then that means it has already been visited
            }
            clone->neighbors.push_back(mappings[child]);
        }
    }
    return clonedNode;
}
