#include <bits/stdc++.h>
using namespace std;

//https://www.lintcode.com/en/old/problem/longest-substring-with-at-most-k-distinct-characters/

class Solution {
   public:
    int lengthOfLongestSubstringKDistinct(string &s, int K) {
        int n = s.length();
        if (n <= K)
            return n;
        unordered_map<char, int> freqMap;
        int maxLen = 0;
        int left = 0, right = 0;
        while (right < n) {
            char c = s[right];
            freqMap[c]++;
            if (freqMap.size() <= K) {
                maxLen = max(maxLen, right - left + 1);
            } else {
                while (left <= right && freqMap.size() > K) {
                    char delChar = s[left++];
                    freqMap[delChar]--;
                    if (freqMap[delChar] == 0)
                        freqMap.erase(delChar);
                }
            }
            right++;
        }
        return maxLen;
    }
};