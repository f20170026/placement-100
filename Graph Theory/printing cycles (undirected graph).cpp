#include <bits/stdc++.h>
using namespace std;
const int N = 100000;
vector<int> graph[N];
void addEdge(int u, int v) {
    graph[u].push_back(v);
    graph[v].push_back(u);
}
/**
* Doesn't handle composite cycles well
* Idea is to store parent of each node and when we find that u---v edge gives a cycle then backtrack the cycle nodes
*/
void dfs_cycle(int u, int p, int color[], int par[]) {
    color[u] = 0;
    par[u] = p;

    for (int v : graph[u]) {
        if (v == p) continue;
        if (color[v] == -1) {
            dfs_cycle(v, u, color, par);
        } else if (color[v] == 0) {
            // cycle forms due to u---v edge
            int node = u;
            while (node != v) {
                cout << node << " ";
                node = par[node];
            }
            cout << v << endl;
        }
    }

    color[u] = 1;
}

// Driver Code
int main() {
    // add edges
    addEdge(1, 2);
    addEdge(2, 3);
    addEdge(3, 4);
    addEdge(4, 6);
    addEdge(4, 7);
    addEdge(5, 6);
    addEdge(3, 5);
    addEdge(7, 8);
    addEdge(6, 10);
    addEdge(5, 9);
    addEdge(10, 11);
    addEdge(11, 12);
    addEdge(11, 13);
    addEdge(12, 13);
    //
    int color[N];
    for (int i = 0; i < N; ++i) color[i] = -1;
    int par[N];
    dfs_cycle(1, 0, color, par);
}
