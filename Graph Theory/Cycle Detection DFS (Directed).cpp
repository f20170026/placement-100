/*
If we are at a node u and we have an edge from u to v but v has been entered but
not exited (that means it is currently in recursion stack) which means u->v is
back-edge and cycle is present
*/

bool dfs(int u, vector<int> adj[], vector<bool> &vis, vector<bool> &recSt) {
    // u is entered
    vis[u] = recSt[u] = true;

    for (int v : adj[u]) {
        if (!vis[v] && dfs(v, adj, vis, recSt))
            return true;
        else if (recSt[v] == true)  // back edge present
            return true;
    }
    // u is exited
    recSt[u] = false;
    return false;
}