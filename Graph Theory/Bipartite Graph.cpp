#include <bits/stdc++.h>
using namespace std;

int V;
vector<vector<int>> adj;
bool isBipartite = false;

/**
 * Do a BFS. Assign the unvisited neighbor opposite color. If current node has a
 * visited neighbor and same color as this node then graph is not 2-colorable (not bipartite)
 * Same thing you can do in DFS also
 */
void bipartite() {
    // color[i] = -1 then unvisited, color[i] = 0 or 1
    vector<int> color(V, -1);
    for (int src = 0; src < V; ++src) {
        if (color[src] == -1) {
            queue<int> q;
            q.push(src);
            color[src] = 0;

            while (!q.empty()) {
                int u = q.front();
                q.pop();
                for (int v : adj[u]) {
                    if (color[v] == -1) {
                        color[v] = 1 - color[u];
                        q.push(v);
                    } else if (color[v] == color[u]) {
                        isBipartite = false;
                        break;
                    }
                }
            }
        }
        if (isBipartite == false) break;
    }
    cout << isBipartite;
}