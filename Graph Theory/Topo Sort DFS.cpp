#include <bits/stdc++.h>
using namespace std;

void dfs(int src, vector<int> adj[], stack<int> &st, bool visited[]) {
    visited[src] = true;
    for (int child : adj[src]) {
        if (!visited[child]) dfs(child, adj, st, visited);
    }
    st.push(src);  // when exited push the node
}

int main() {
    int V, E;
    cin >> V >> E;
    vector<int> adj[V];
    int edge_ct = E;
    while (edge_ct--) {
        int a, b;
        cin >> a >> b;
        adj[a].push_back(b);
    }
    stack<int> st;
    bool visited[V] = {false};
    for (int i = 0; i < V; ++i) {
        if (!visited[i]) dfs(i, adj, st, visited);
    }
    while (!st.empty()) {
        int node = st.top();
        st.pop();
        cout << node << " ";
    }
}