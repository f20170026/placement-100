#include <bits/stdc++.h>
using namespace std;

/**
* Best tutorial => https://codeforces.com/blog/entry/22276
* Relaxing adjacent vertices like dijkstra =>
* If you are at u and wt(u, v) = 0 then they are on same level so add it to front
* if wt(u, v) = 1 then v is on next level so add it to back.
*/
void 0_1_BFS(int src) {
    int V;
    vector<vector<int>> adj;

    vector<int> dist(V, INF);
    vector<int> parent(V, -1);
    deque<int> q;

    dist[src] = 0;
    parent[src] = -1;
    q.push_front(src);
    while (!q.empty()) {
        int u = q.front();
        q.pop_front();

        for (int v : adj[u]) {
            // relax adj vertices
            if (dist[v] > dist[u] + weight(u, v)) {
                if (weight(u, v) == 0) {
                    // u and v are on same level
                    q.push_front(v);
                } else {
                    q.push_back(v);
                }
                parent[v] = u;
            }
        }
    }

    //Do the path tracing using parent array
}
