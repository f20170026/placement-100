#include <bits/stdc++.h>
using namespace std;
/**
* Dijkstra relies on => if weights are non-negative then adding an edge can never
* make a path shorter. that's why always pick shortest dist node works. Works only
* on non-negative directed/undirected graph. shortest path doesn't change if each
* edge is multiplied with same constant but changes if each edge is added with
* same constant (due to different number of edges)
* Two Implementations =>
* 1. For Dense Graphs where E ~ O(V^2)
* Run 'V' iterations. In each iteration pick an unvisited node with min distance and relax all it's adjacent nodes 
* a) Picking min distance node = O(V) b)
* relaxing all it's adjacent nodes = O(E1) Total Time Complexity = V^2 + E1 + E2
* +.... = O(V^2 + E)

* 2. For Sparse Graphs where E << O(V^2) so O(V^2 + E) is bad version.
* a) Picking min distance node = O(logV)
* b) Relaxing all it's adjacent nodes = O(E1logV) since we insert the node in the
* data structure if distance changes Total = O(VlogV + ElogV) But when relaxing if
* distance changes need to remove it from data structure and insert it. Removing
* any node is supported in set which takes O(logV). So at any point of time in set
* there are O(V) nodes But removing arbitary nodes is not supported in priority queue STL. 
* So what we do is just insert it without removing. This doesn't change the optimal
* answer but total nodes present in heap will be O(E). Implement using set =>
* O(VlogV + ElogV) Implement using heap => O(VlogE + ElogE).
* Custom Heap we can write modifyKey() function in O(LogV) so we get O(VlogV + ElogV)

* Path tracing =>
* Can do path tracing same like bfs
*/
vector<int> path_tracing(int source, int dest, vector<int> parent) {
    vector<int> path;
    for (int v = dest; v != -1; v = parent[v]) {
        path.push_back(v);
    }
    reverse(path.begin(), path.end());
    return path;
}

const int INF = 1000000000;
vector<vector<pair<int, int>>> adj;

// Dense Graph
void dijkstra1(int source, vector<int> dist, vector<int> parent) {
    int V = adj.size();
    dist.resize(V, INF);
    parent.resize(V, -1);
    vector<bool> vis(V, false);

    dist[source] = 0;
    for (int i = 0; i < V; ++i) {
        // pick min distance unvisited node
        int u = -1;
        for (int j = 0; j < V; ++j) {
            if (vis[j] == false && (u == -1 || dist[u] > dist[j])) u = j;
        }
        vis[u] = true;
        // relax all adjcanet nodes
        for (auto edge : adj[u]) {
            int v = edge.first;
            int wt = edge.second;

            if (dist[v] > dist[u] + wt) {
                dist[v] = dist[u] + wt;
                parent[v] = u;
            }
        }
    }
}

// Sparse Graphs

// Set
void dijkstra2(int source, vector<int> &dist, vector<int> &parent) {
    int V = adj.size();
    dist.resize(V, INF);
    parent.resize(V, -1);

    dist[source] = 0;
    set<pair<int, int>> q;  //(dist, node) = set will be ordered according to dist
    while (!q.empty()) {
        // getting min unvisited node
        int u = q.begin()->second;
        q.erase(q.begin());  // O(logV)

        for (auto edge : adj[u]) {
            int v = edge.first, wt = edge.second;
            if (dist[v] > dist[u] + wt) {
                q.erase({dist[v], v});  // O(logV)
                dist[v] = dist[u] + wt;
                parent[v] = u;
                q.insert({dist[v], v});  // O(logV)
            }
        }
    }
}

// Min Heap
void dijkstra2(int source, vector<int> &dist, vector<int> &parent) {
    int V = adj.size();
    dist.resize(V, INF);
    parent.resize(V, -1);

    dist[source] = 0;
#define ppi pair<int, int>
    priority_queue<ppi, vector<ppi>, greater<ppi>> q;  //(dist, node) = set will be ordered according to dist
    while (!q.empty()) {
        // getting min unvisited node
        int u = q.top()->second;
        int d_u = q.top()->first;
        q.pop();  // O(logE)

        // looking at old pair so continue
        if (d_u != dist[u]) continue;
        for (auto edge : adj[u]) {
            int v = edge.first, wt = edge.second;
            if (dist[v] > dist[u] + wt) {
                dist[v] = dist[u] + wt;
                parent[v] = u;
                q.push({dist[v], v});  // O(logE) E due to multiple pairs
            }
        }
    }
}
