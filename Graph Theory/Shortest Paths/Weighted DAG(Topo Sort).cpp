#include <bits/stdc++.h>
using namespace std;

/**
* Shortest path from source to all other nodes in a DAG.
* Use Topological Sorting O(V + E)
*/

int V;
vector<vector<int>> adj;
stack<int> st;  // will contain topo order of graph in descending order of exit time
vector<bool> visited(V, false);

void dfs_topoSort(int u) {
    // enterd node
    visited[u] = true;
    for (int v : adj[u]) {
        dfs_topoSort(u);
    }
    // exitting node
    st.push(u);
}

void shortest_path(int src) {
    // 1. Topo Sort of the graph
    for (int u = 0; u < V; ++u) {
        if (!visited[u]) {
            dfs_topoSort(u);
        }
    }

    // 2. Finding shortest path distance
    int dist[V] = {INF};
    dist[src] = 0;
    int parent[V] = {-1};

    while (!st.empty()) {
        int u = st.top();
        st.pop();
        for (int v : adj[u]) {
            if (dist[v] > dist[u] + weight(u, v)) {
                dist[v] = dist[u] + weight(u, v);
                parent[v] = u;
            }
        }
    }
}