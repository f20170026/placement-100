#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> adj;  // adjacency list representation
int n;                    // number of nodes
vector<bool> visited(n);

// Simple Implementation
void dfs(int u, vector<int> path) {
    visited[u] = true;
    print(path);  // prints the path from origin source to this u vertex

    for (int v : adj[u]) {
        if (!visited[v]) {
            path.push_back(v);
            dfs(v, path);
            path.pop_back();
        }
    }
}

void dfsEntireGraph() {
    for (int i = 0; i < n; ++i) {
        if (!visited[i]) {
            dfs(i, {i});
        }
    }
}