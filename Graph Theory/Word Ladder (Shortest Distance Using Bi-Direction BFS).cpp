#include <bits/stdc++.h>
using namespace std;

/**
 * Shortest Path Using BFS
 */
int ladderLength(string beginWord, string endWord, vector<string> &wordList) {
    dict.clear();
    for (string word : wordList) dict.insert(word);

    // bfs(beginWord, endWord)
    queue<string> q;
    unordered_set<string> visited;

    q.push(beginWord);
    visited.insert(beginWord);
    int level = 0;
    while (!q.empty()) {
        int size = q.size();
        while (size--) {
            string node = q.front();
            q.pop();
            if (node == endWord) return level + 1;

            vector<string> children = generateChildren(node);
            for (string child : children) {
                if (visited.find(child) == visited.end()) {
                    visited.insert(child);
                    q.push(child);
                }
            }
        }
        level++;
    }

    return 0;
}
/**
 * Shortest Path Using Bi-Directional BFS
 */
unordered_set<string> dict;

vector<string> generateChildren(string s) {
    vector<string> children;
    for (int i = 0; i < s.length(); ++i) {
        string temp = s;
        for (char c = 'a'; c <= 'z'; ++c) {
            if (s[i] == c) continue;
            temp[i] = c;
            if (dict.find(temp) != dict.end()) children.push_back(temp);
        }
    }
    return children;
}

void BFS(queue<string> &q, unordered_set<string> &visited,
         unordered_map<string, int> &dist) {
    string node = q.front();
    q.pop();
    vector<string> children = generateChildren(node);
    for (string child : children) {
        if (visited.find(child) == visited.end()) {
            visited.insert(child);
            q.push(child);
            dist[child] = dist[node] + 1;
        }
    }
}
int ladderLength(string beginWord, string endWord, vector<string> &wordList) {
    dict.clear();
    for (string word : wordList) dict.insert(word);

    // Base Case
    if (dict.find(endWord) == dict.end()) return 0;
    if (beginWord == endWord) return 1;

    queue<string> f, b;
    unordered_set<string> visitedF, visitedB;
    unordered_map<string, int> distF, distB;

    f.push(beginWord);
    distF[beginWord] = 0;
    b.push(endWord);
    distB[endWord] = 0;
    visitedF.insert(beginWord);
    visitedB.insert(endWord);

    while (!f.empty() && !b.empty()) {
        BFS(f, visitedF, distF);
        BFS(b, visitedB, distB);

        // check for intersection
        int ans = INT_MAX;
        for (auto it : visitedF) {
            if (visitedB.find(it) != visitedB.end()) {
                ans = min(ans, distF[it] + distB[it] + 1);
            }
        }
        if (ans != INT_MAX) return ans;
    }

    return 0;
}