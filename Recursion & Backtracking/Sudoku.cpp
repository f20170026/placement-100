bool isSafe(int row, int col, int number, vector<vector<char>> &A) {
    // check in the row
    for (int j = 0; j < 9; ++j) {
        if (A[row][j] == (number + '0')) return false;
    }
    // check in the col
    for (int i = 0; i < 9; ++i) {
        if (A[i][col] == (number + '0')) return false;
    }

    // check in the same box
    int x = row / 3 * 3;
    int y = col / 3 * 3;
    for (int i = x; i < x + 3; ++i) {
        for (int j = y; j < y + 3; ++j) {
            if (A[i][j] == (number + '0')) return false;
        }
    }

    return true;
}

bool backtrack(vector<vector<char>> &A) {
    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (A[i][j] == '.') {
                // fill this position
                for (int number = 1; number <= 9; ++number) {
                    if (isSafe(i, j, number, A)) {
                        A[i][j] = number + '0';
                        if (backtrack(A)) return true;
                        A[i][j] = '.';
                    }
                }
                // if it is not possible to fill this position with any number
                // then we can't solve the sudoku
                return false;
            }
        }
    }
    return true;
}

void solveSudoku(vector<vector<char>> &A) { backtrack(A); }
