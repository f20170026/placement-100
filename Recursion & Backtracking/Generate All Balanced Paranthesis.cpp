#include <bits/stdc++.h>
using namespace std;

/**
 * 3 Keys of backtracking
 * 1. our choice = place ( or )
 * 2. our constraints = CANT CLOSE UNTIL WE OPEN. So count of ( matters
 * 3. our goal = need 2*N placements
 * Time Complexity = O(2^N) = loose bound. we use pruning
 */

vector<string> result;

void backtrack(int openCt, int closeCt, int ind, int N, string str) {
    if (openCt + closeCt == 2 * N) {
        result.push_back(str);
        return;
    }

    //Place ( at str[ind]
    if (openCt < N) {
        str[ind] = '(';
        backtrack(openCt + 1, closeCt, ind + 1, N, str);
    }

    //Place ) at str[ind]
    if (closeCt < N && openCt > closeCt) {
        str[ind] = ')';
        backtrack(openCt, closeCt + 1, ind + 1, N, str);
    }
}

vector<string> generateParenthesis(int A) {
    result.clear();
    string temp = "";
    for (int i = 0; i < 2 * A; ++i)
        temp += ".";

    backtrack(0, 0, 0, A, temp);
    return result;
}
