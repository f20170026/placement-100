#include <bits/stdc++.h>
using namespace std;

void swap(string &str, int i, int j) {
    char temp = str[i];
    str[i] = str[j];
    str[j] = temp;
}

/*
Naive Solution ->
Generate all permutations of string. When you are printing the permutation check
if AB is present in the string. O(2^N).
*/
void rec_1(string str, int ind = 0) {
    if (ind == (str.length() - 1)) {
        if (str.find("AB") == string ::npos) cout << str << endl;
        return;
    }

    for (int i = ind; i < str.length(); ++i) {
        swap(str, ind, i);
        rec_1(str, ind + 1);
        swap(str, ind, i);
    }
}

/*
Optimized Solution ->
Don't generate all permutations.
Cut Down Some Recursive Calls
*/
bool isSafe(string str, int ind, int i) {
    // Return false if AB can occur by swapping ind & i
    if (ind != 0 && str[ind - 1] == 'A' && str[i] == 'B') return false;
    if (ind + 1 == str.length() - 1 && str[ind] == 'B' && str[ind + 1] == 'A')
        return false;
    return true;
}
void rec_2(string str, int ind = 0) {
    if (ind == (str.length() - 1)) {
        cout << str << endl;
        return;
    }

    for (int i = ind; i < str.length(); ++i) {
        if (isSafe(str, ind, i)) {
            swap(str, ind, i);
            rec_1(str, ind + 1);
            swap(str, ind, i);
        }
    }
}

int main() {
    string str;
    cin >> str;
    rec_1(str);
}