distance between 2 nodes in tree
https://leetcode.com/problems/check-completeness-of-a-binary-tree/submissions/ (simple level order bfs)
* https://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion-and-without-stack/ ( Morris Traversal. Check this)
* https://www.geeksforgeeks.org/find-if-a-degree-sequence-can-form-a-simple-graph-havel-hakimi-algorithm/
https://www.geeksforgeeks.org/convert-the-undirected-graph-into-directed-graph-such-that-there-is-no-path-of-length-greater-than-1/?ref=rp
* https://www.geeksforgeeks.org/print-nodes-at-k-distance-from-root/
* https://practice.geeksforgeeks.org/problems/node-at-distance/1/?track=PC-W6-T&batchId=154

**TREE** <br>
- For tree recursion carefully think about base cases and then write them. work it out on paper and then write. dont just write blindly.
- Trees = two types of recursion (top-down , bottom-up)
Bottom-up = Mostly optimization problems on binary trees use this
- First calculating answer of leaves then going up
- If answer can be found in subtrees itself not always at root then use bottom-up recursion. 
- Storing answer as ref. variable but passing appropriate results to above parent.
- This recursion is basically bottom -up traversal (passing info about subtrees to parent). 

- Two recursions = Top-down and bottom-up
Use bottom-up for optimization over top-down approach

Check if graph is tree =
1. if graph is connected and has no cycle

Constructing Tree =
1. From parent array (1st create all value nodes and then link all the nodes)
2. From Traversals (**IMP**)
3. https://www.geeksforgeeks.org/check-if-given-preorder-inorder-and-postorder-traversals-are-of-same-tree/?ref=rp (**IMP**)
4. https://www.geeksforgeeks.org/print-postorder-from-given-inorder-and-preorder-traversals/?ref=rp (**IMP**)

Views =
1. Left View Of Tree, Right View Of Tree
2. Vertical Traversal, Top View, Bottom View Of Tree

Bottom-up = (Essentialy a post-order type. left, right and then compute at root)
- https://www.geeksforgeeks.org/lowest-common-ancestor-binary-tree-set-1/
- Diamater of binary tree
- Max Path Sum from any node to any node
- (**IMP**)Max Path Sum from leaf to leaf
- Diameter of N-Ary tree

(**IMP**)Similar questions (returning some data. that data might have many attributes so use some struct)
- https://www.geeksforgeeks.org/print-nodes-distance-k-given-node-binary-tree/
- https://www.geeksforgeeks.org/minimum-time-to-burn-a-tree-starting-from-a-leaf-node/?ref=leftbar-rightbar
- https://www.geeksforgeeks.org/find-the-largest-subtree-in-a-tree-that-is-also-a-bst/
- https://www.geeksforgeeks.org/count-the-number-of-binary-search-trees-present-in-a-binary-tree/ (Same Like Above)

https://www.geeksforgeeks.org/count-bst-subtrees-that-lie-in-given-range/


- https://www.geeksforgeeks.org/check-weather-given-binary-tree-perfect-not/
First Depth of root. Every leaf should have same depth and every node should have two children



Traversals =
1. Traversals Without Recursion
2. Inorder & Preorder Traversal Without Recursion & Stack = Morris Traversalon
3. Level Order Spiral
4. Reverse Level Order
5. Reverse Level Order (Spiral) 


TODO =>
https://www.geeksforgeeks.org/diameter-n-ary-tree/
https://www.geeksforgeeks.org/longest-path-undirected-tree/ (diamter in n-ary check also)
https://www.geeksforgeeks.org/serialize-deserialize-binary-tree/ (See this before interview)
https://www.interviewbit.com/problems/populate-next-right-pointers-tree/  (IMP without extra space)
https://www.geeksforgeeks.org/boundary-traversal-of-binary-tree/
https://www.interviewbit.com/problems/remove-nodes-from-path-sum-less-than-b/?ref=random-problem (Nice Problem on recursion)
https://www.geeksforgeeks.org/find-height-binary-tree-represented-parent-array/
https://www.geeksforgeeks.org/check-if-inorder-traversal-of-a-binary-tree-is-palindrome-or-not/?ref=leftbar-rightbar (solve pre-req problems also)
https://practice.geeksforgeeks.org/problems/check-mirror-in-n-ary-tree/0

LCA (binary tree, bst)


vertical width vs max width (check gfg questions) vs Maximum Width of Binary Tree (leetcode)

https://www.geeksforgeeks.org/print-all-the-levels-with-odd-and-even-number-of-nodes-in-it-set-2/