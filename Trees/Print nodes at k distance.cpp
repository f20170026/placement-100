#include "Tree.h"

void KDistanceNodesDown(Node* root, int dist, int K, vector<int>& nodes) {
    if (!root || dist > K) return;
    if (dist == K) {
        nodes.push_back(root->data);
    }
    KDistanceNodesDown(root->left, dist + 1, K, nodes);
    KDistanceNodesDown(root->right, dist + 1, K, nodes);
}

//Return dist of target from this node
int KDistanceNodesMain(Node* root, int K, int target, vector<int>& nodes) {
    if (!root) return -1;
    if (root->data == target) {
        KDistanceNodesDown(root, 0, K, nodes);
        return 0;
    }
    int leftDist = KDistanceNodesMain(root->left, K, target, nodes);
    int rightDist = KDistanceNodesMain(root->right, K, target, nodes);
    if (leftDist != -1) {
        //in left subtree we found target
        //leftDist+1 is dist from root to target
        if (leftDist + 1 == K)
            nodes.push_back(root->data);
        else
            KDistanceNodesDown(root->right, 0, K - leftDist - 2, nodes);

        return leftDist + 1;
    }
    if (rightDist != -1) {
        //in right subtree we found target
        //rightDist+1 is dist from root to target
        if (rightDist + 1 == K)
            nodes.push_back(root->data);
        else
            KDistanceNodesDown(root->left, 0, K - rightDist - 2, nodes);

        return rightDist + 1;
    }
    return -1;
}

vector<int> KDistanceNodes(Node* root, int target, int k) {
    vector<int> result;
    KDistanceNodesMain(root, k, target, result);
    sort(result.begin(), result.end());
    return result;
}