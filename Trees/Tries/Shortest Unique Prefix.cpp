#include <bits/stdc++.h>
using namespace std;

//https://www.interviewbit.com/problems/shortest-unique-prefix/?ref=random-problem

const int MAX_CHARS = 26;

class Trie {
   public:
    bool isEnd;
    Trie* child[MAX_CHARS];
    bool isShared;  //if it is true then this node is shared by more than one nodes. So this prefix ca't be unique
    Trie() {
        isShared = false;
        for (int i = 0; i < MAX_CHARS; ++i) {
            child[i] = NULL;
        }
    }
    void insert(string word) {
        if (word.empty()) {
            return;
        }

        Trie* current = this;
        for (char c : word) {
            if (current->child[c - 'a'] == NULL) {
                current->child[c - 'a'] = new Trie();
            } else {
                current->child[c - 'a']->isShared = true;
            }
            current = current->child[c - 'a'];
        }
        current->isEnd = true;
    }
    string getShortestUniquePrefix(string word) {
        string prefix = "";
        Trie* current = this;
        for (char c : word) {
            prefix += c;
            //if not shared
            if (current->child[c - 'a']->isShared == false) {
                return prefix;
            }
            current = current->child[c - 'a'];
        }
        return prefix;
    }
};

vector<string> Solution::prefix(vector<string>& A) {
    vector<string> result;
    Trie* obj = new Trie();
    for (string word : A) {
        obj->insert(word);
    }
    for (string word : A) {
        result.push_back(obj->getShortestUniquePrefix(word));
    }
    return result;
}
