#include "Tree.h"

/**
 * In postorder first print left subtree first and then print right subtree and then root
 * Time Complexity O(N^2)
 * Can be improved using unordered_map to return index in O(1)
 */

void printPostorder(int inorder[], int preorder[], int N) {
    //preorder[0] is root. Search its index in inorder

    if (N == 0)
        return;
    if (N == 1)
        return preorder[0];

    int index;
    for (index = 0; index < N; ++index)
        if (inorder[index] == preorder[0])
            break;

    printPostorder(inorder, preorder + 1, index);
    printPostorder(inorder + index + 1, preorder + 1 + index, N - 1 - index);
    cout << preorder[0] << " ";
}