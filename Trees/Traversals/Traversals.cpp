#include "Tree.h"

vector<int> inorderTraversal(Node *root) {
    if (root == NULL)
        return {};

    vector<int> inorder;
    stack<Node *> st;
    st.push(root);
    while (!st.empty()) {
        Node *node = st.top();
        //NULL indicates we need to push right subtree of the top node
        if (node == NULL) {
            st.pop();
            //Entire traversal is done
            if (st.empty())
                break;
            node = st.top();
            st.pop();
            inorder.push_back(node->val);
            st.push(node->right);
        } else {
            st.push(node->left);
        }
    }
    return inorder;
}

vector<int> preorderTraversal(Node *root) {
    if (root == NULL)
        return {};

    stack<Node *> st;
    vector<int> preorder;
    st.push(root);
    while (!st.empty()) {
        Node *node = st.top();
        st.pop();
        preorder.push_back(node->val);
        if (node->right)
            st.push(node->right);
        if (node->left)
            st.push(node->left);
    }
    return preorder;
}

/**
 * Postorder is same as preorder except we add nodes from back and also visit right first and then left
 * Left Right Val. Val first then go to right then go to left. And reverse the order :)
*/
vector<int> postorderTraversal(Node *root) {
    if (root == NULL)
        return {};

    stack<Node *> st;
    vector<int> postorder;
    st.push(root);

    while (!st.empty()) {
        Node *node = st.top();
        st.pop();
        postorder.push_back(node->val);
        if (node->left)
            st.push(node->left);
        if (node->right)
            st.push(node->right);
    }
    reverse(postorder.begin(), postorder.end());
    return postorder;
}
