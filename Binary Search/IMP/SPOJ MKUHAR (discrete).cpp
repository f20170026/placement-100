//https://www.spoj.com/problems/MKUHAR/

#include <climits>
#include <cmath>
#include <iostream>
#include <vector>
using namespace std;

struct Item {
    int amountNeeded;
    int amountAvailable;
    int smallPacketSize;
    int smallPacketPrice;
    int largePacketSize;
    int largePacketPrice;
    Item(int x, int y, int sm, int pm, int sv, int pv) {
        amountNeeded = x;
        amountAvailable = y;
        smallPacketSize = sm;
        smallPacketPrice = pm;
        largePacketSize = sv;
        largePacketPrice = pv;
    }
};

//Return true if can serve "servingsNeeded" number of servings
bool isPossible(int servingsNeeded, vector<Item> &items, int itemsCount, int dollars) {
    int totalPrice = 0;
    for (Item item : items) {
        int amountNeeded = item.amountNeeded * servingsNeeded - item.amountAvailable;
        //Don't need to buy anything extra
        if (amountNeeded <= 0)
            continue;
        //There are two types of items => small sized and larged size. Need to find the minimum price we get for all the combinations
        int minPrice = INT_MAX;
        //smallPacketSize * smallPacketCount + largePacketSize * largePacketCount >= amountNeeded
        int limit = (int)ceil((1.0 * amountNeeded) / (1.0 * item.smallPacketSize));  //If you take more small packets than this then you get more amount
        for (int smallPacketCount = 0; smallPacketCount <= limit; ++smallPacketCount) {
            int amountLeft = amountNeeded - smallPacketCount * item.smallPacketSize;
            int largePacketCount = 0;
            if (amountLeft > 0) {
                largePacketCount = (int)ceil((1.0 * amountLeft) / (1.0 * item.largePacketSize));
            }
            minPrice = min(minPrice, smallPacketCount * item.smallPacketPrice + largePacketCount * item.largePacketPrice);
        }
        totalPrice += minPrice;
        if (totalPrice > dollars)
            return false;
    }
    return totalPrice <= dollars;
}

//Find largest servings can be done. Each servings need all the ingredients
int largestServings(vector<Item> &items, int itemsCount, int dollars) {
    //Binary search on "number of servings"
    int low = 0;
    int high = dollars + 1000;  //very loose upper bound
    int ans = -1;
    while (low <= high) {
        int mid = low + (high - low) / 2;
        if (isPossible(mid, items, itemsCount, dollars)) {
            ans = max(ans, mid);
            low = mid + 1;
        } else {
            high = mid - 1;
        }
    }
    return ans;
}

int main() {
    int itemsCount, dollars;
    cin >> itemsCount >> dollars;
    vector<Item> items;
    for (int i = 0; i < itemsCount; ++i) {
        int x;
        int y;
        int sm;
        int pm;
        int sv;
        int pv;
        cin >> x >> y >> sm >> pm >> sv >> pv;
        Item item(x, y, sm, pm, sv, pv);
        items.push_back(item);
    }
    cout << largestServings(items, itemsCount, dollars) << endl;
}