## **Dynamic Programming**

DP Resources <br>
* https://www.facebook.com/utkarsh028/posts/113560319994926
* https://qr.ae/pNtB42
* https://qr.ae/pNvCJF
* https://www.codechef.com/wiki/tutorial-dynamic-programming (codechef tut)
* https://codeforces.com/blog/entry/20935 (DP On Trees)
* https://icpc.ninja/Algorithms/Tree/DP/ (Tree DP)
* https://cs.stackexchange.com/questions/119707/thought-process-to-solve-tree-based-dynamic-programming-problems (Tree DP IMP)
* https://www.iarcs.org.in/inoi/online-study-material/topics/dp-trees.php
* https://blogarithms.github.io/articles/2019-10/inout-dp-tree (in-out dp)
* https://codeforces.com/blog/entry/67679 (Tutorial)
* https://codeforces.com/blog/entry/8219 (Optimization)
* https://codeforces.com/blog/entry/16437 (Link to some Blogs)
* https://codeforces.com/contest/455/problem/A

```
Steps to solve DP problem :-
1. Create recursive backtrack solution.
2. Avoid redundant arguments in the recursive function.
If arguments can be created using other arguments then
they are redundant.
3. Minimize the range of possible values of function arguments.
Range of possible values the function arguments can get from a valid input
4. Try to optimize the time complexity of one function call (remember, you can treat recursive calls as they would run in O(1) time)
5. Cache the values and don't calculate the same things twice
6. Final Time Compleixty =
Range Of Possible Values The Function Can Be Called With
                        *
Time Complexity Of One Function Call
7. Converting Top-Down to Bottom-Up
- if in recursion index reduces from N to 0(in base-case)
use loop from 1 to N (exclude base-case. Initialize base-case before the loop starts)
- return top-down === break bottom-up
8. If arguments cant be reduced further but still you want to optimize then think
of preprocessing.
9. Think of first or last in the sequence to get idea
10. Think of including or excluding element in choice diagram
11. If you can solve from first to last or last to first then use 1D dp, else two pointers, 2D Dp.
[0....i] [i....N-1] [i....j]
12. When N is less and it might be DP then it is DP + Bitmasking
```

IMP TIPS =>
- Constructing solution set =
If in DP, you are not just asked to compute value but also the set of numbers which form the solution.
Then first compute the value and then use the matrix to form the solution set.
- Indexes in recurrence ->
opt(i) = 0....i (including ith ele or not)
opt(i) = i....n-1
opt(i, j) = i...j


OPTIMAL SUBSTRUCTURE
If problem can be solved using sub problems then use recursion.
So if the solution to a problem can be formed using solution of sub-problem then start forming the recurrence relation and use recursion
Now see if you can optimize over the plain recursion. If there are overlapping subproblems.


DP try question with examples. You might get the recurrence and pattern.


Preprocessing some things really reduces complexity.

Think of optimizing both in time & space.
If used 2D dp, try to use 1D dp.
If N^3 solution on 2D dp then try to think of N^2 solution.
If recurennce depends on only one previous value then use one variable
If recurennce depends on only two previous value then use two variable

Tips for top_down :
if(dp[x][y] != -1) we are using this condition
but if dp[x][y] can take any value then take visited array
and use vis[x][y]

Remember all the important questions. Most questions are variations of these questions
0-1 Knapsack, Unbounded Knapsack, Fibonacci, LCS, LIS, Kadane's Algo, Matrix Chain Multiplication, DP on Trees, DP on grid/matrix.

## Base Questions =>

1. 0-1 Knapsack (https://www.geeksforgeeks.org/space-optimized-dp-solution-0-1-knapsack-problem/)
* O(N*W) Time & Space
* O(N*W) Time | O(W) space using 2 Arrays
* O(N*W) Time | O(W) space using 1 Arrays [going from sum to 0 in second loop whereas in unbounded we from 0 to sum only]

Identify =
- If question contains subsets + sum then think in terms of 0-1 Knapsack.
- subsets + optimization = DP (0-1 Knapsack)
- count + sets + given sum/diff = DP (0-1 Knapsack)
- paritioning the sets into two subsets = DP

Variation =
1. Subset Sum Problem, Printing all subsets with given sum
2. Equal Sum Parition Problem = Parition Array into two subsets
3. Count Number Of Subsets with a given sum
4. (**IMP**)Min Subset Diff. = Paritition array into 2 subsets s.t absolute diff b/w their sums must be min
5. Count Number of subset partitions with given difference
6. (**IMP**)Target Sum = No of ways to arrange +, - to array to get a given sum
7. (**IMP**)3D DP. While solving for choice diagram you will get s1 = n1 * Avg_Set. s1 & n1 are variables. 
https://www.geeksforgeeks.org/partition-an-array-of-non-negative-integers-into-two-subsets-such-that-average-of-both-the-subsets-is-equal/

2. Unbounded Knapsack
* Here an item can be selected any number of items and place it in knapsack
* Very IMP difference b/w Rod Cutting & Knapsack is
in knapsack total wt of items must be <= Capacity of knapsack but
in rod cutting total length of segments must be exactly length of rod. So you have to add one more important condition.
For that condition check (Maximize Cut Segments Code)
You have to add this condition in questions where you include the current item and add some constant to the value of sub-problem
like 1 + (result of sub-problem). Or else you can initialize with proper base case to avoid writing that condition.
In case of count of subsets with given sum, you directly use (result of sub-problem) so there you dont need to add any condition.
* Knapsack = first row & first column => 0
* Subset Sum = first row = false && first column => true
* Rod Cutting = first row = -INF && first column => 0
Basically first row is for N == 0 (0 items). So you can ignore that and directly fill the second row without using 1st row values like Tushar Roy does for rod cutting

Variations =
1. Rod Cutting Problem
2. Coin Change = No of ways to make sum, Min. no of coins to make sum
Base Case = IMP.

3. Longet Common Subsequence (LCS)

Identify =
* 2 strings and some optimal question (LCSubstring, SCS, Min # Insertions & Deletions from a to b)
* Even 1 String but something related to subsequence (LPSubsequence) Then you need to see how you can get 2 strings from that 1 string and then apply LCS
* Insertions/Deletions + Palindrome - Think in terms of Longest Palindromic Subsequence

Variations =
1. Print LCS
2. Shortest Common Supersequence & Printing SCS
3. Min # Of insertions & deletions to convert A to B
4. Edit Distance
5. Longest Palindromic Subsequence
6. (**IMP**)Min # of deletions/insertions in string to make it a palindrome
7. (**IMP**)Longest Repeating Subsequence (https://www.geeksforgeeks.org/longest-repeating-subsequence/)
8. Sequence Pattern Matching = Another Imp Pattern (Many problems can be solved using this pattern)
Can be solved by DP. But optimized approach is different.
9. (**IMP**)Counting subsequences
0.......i
0.......j
(i,j) = (i-1,j) + (i,j-1) but here (i-1,j-1) is counted twice
so when s[i]!=s[j] we need to substract by (i-1,j-1)
when s[i]==s[j] we don't need to substract since we will count them as different set of subsequences by attaching the s[i]
https://www.geeksforgeeks.org/count-common-subsequence-in-two-strings/
https://www.geeksforgeeks.org/count-palindromic-subsequence-given-string/

4. Matrix Chain Multiplication (Top Down better than Bottom Up)
[i...j] = i represents ending dimension of starting matrix, j represents ending dimension of ending matrix

Identify =
parenthesizing/putting brackets + min/max/# of ways = MCM variation

Variations =
1. Printing MCM (See Your Code). Good One. https://practice.geeksforgeeks.org/problems/brackets-in-matrix-chain-multiplication/0/
2. (**IMP**)Palindrome Partitioning =
Following standard MCM approach = O(N^3). Can optimize it.
3. (**IMP**)Boolean Paraenthesization = Tricky Solution. 
4. (**IMP**)Scramble String = Very Complex Problem
5. (**IMP**)https://leetcode.com/problems/burst-balloons/ = Very Good Problem
3 1 2
Bursting balloon 1 = 3 * 1 * 2 coins think of as multiplying (3*1) & (1*2) matrices
https://leetcode.com/problems/burst-balloons/discuss/531519/MATRIX-CHAIN-MULTIPLICATION [**IMP**]
6. (**IMP**)Egg Dropping Problem

5. Longest Increasing Subsequence
https://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/

Vartions =
1. (**IMP**)https://www.geeksforgeeks.org/dynamic-programming-building-bridges/

6. Kadane's Algo
* (**IMP**)https://www.geeksforgeeks.org/maximum-sum-subarray-removing-one-element/
* https://www.geeksforgeeks.org/maximum-sum-2-x-n-grid-no-two-elements-adjacent/ [Check you IB Solution]

7. DP On Grid

Tips = 
- In grid dp, try both ways go from top left to bottom right manner (answer will be dp[n-1][m-1]) and also from bottom right to top left manner (answer will be dp[0][0])

1. https://www.geeksforgeeks.org/min-cost-path-dp-6/
2. Max Falling Sum = https://practice.geeksforgeeks.org/problems/path-in-matrix/0
3. https://www.geeksforgeeks.org/find-the-longest-path-in-a-matrix-with-given-constraints/
4. (**IMP**)https://www.interviewbit.com/problems/dungeon-princess/ (Grid DP) (Very IMPPP) = only works when we calc from Bottom right to top left. 
Doesn't work if we calculate from top left to bottom right. Direction in which we calculate the answer also matters. Explanation = https://tinyurl.com/ya7oueud
5. https://www.geeksforgeeks.org/minimum-positive-points-to-reach-destination/ (Dungeon Princess)
6. https://www.geeksforgeeks.org/count-possible-paths-top-left-bottom-right-nxm-matrix/

8. DP + Bitmasking
Tips = 
- Try to reduce arguments (maybe can dervie other args from set bits/unset bits from mask)


1. (**IMP**)https://www.geeksforgeeks.org/travelling-salesman-problem-set-1/
2. (**IMP**)Assignment Problem (Try to think of any redundant arguments using the tip)
3. Check for hamiltonian path in graph


9. Digit DP

* https://www.interviewbit.com/problems/numbers-of-length-n-and-value-less-than-k/


DP Problems <br>
* https://www.geeksforgeeks.org/mobile-numeric-keypad-problem-set-2/
* https://www.geeksforgeeks.org/painters-partition-problem/
* (**IMP**)https://leetcode.com/problems/ones-and-zeroes/ [Greedy or DP]
* https://practice.geeksforgeeks.org/problems/stickler-theif/0
* https://www.interviewbit.com/problems/maximum-size-square-sub-matrix/
* (**IMP**)https://www.geeksforgeeks.org/minimum-increment-or-decrement-required-to-sort-the-array-top-down-approach/?ref=leftbar-rightbar

String DP <br>
if string dp then identify if one index is sufficient in state or 2 index are required. The following are common :-
[0...i]
[i...n-1]
[i...j]
palindrome partitioning <----> word break (see the pattern)
- https://www.geeksforgeeks.org/split-the-given-string-into-primes-digit-dp/
## Breaking Words DP & Backtracking (Similar Problems)
1. is it possible? (DP)
2. min cuts needed (DP)
3. all possible cuts (DP + backtrackings)
- https://leetcode.com/problems/word-break/ (Is it possible?)
- https://practice.geeksforgeeks.org/problems/palindromic-patitioning/0 (Minimum Cuts)
- (**IMP**)https://leetcode.com/problems/word-break-ii/ (all possible cuts)
- (**IMP**)https://www.interviewbit.com/problems/palindrome-partitioning/?ref=similar_problems (all possible parititions)

Problems = 
* Longest Palindromic Substring =
1. Just checking longest common substring between the string & reversed doesn't work.
Ex:- str = abcdefcba    rev(str) = abcfedcab
Longest Common Substring is abc but that isn't a palindrome. So fails when the reverse of non-palindromic substring is also present in the string.
2. DP Solution = O(N^2) Time | O(N^2) Space. Can Be Reduced To O(N) space
3. O(N^2) Time | O(1) Space.
Expanding From Middle. 
For even palindromes we should do expandFromMiddle(i, i)
For odd palindromes we should do expandFromMiddle(i, i + 1)
where expandFromMiddle expands till the characters on either side of mirror are equal.
For all indices do this so O(N^2) Time.
```
int expandFromMiddle(string str, int &left, int &right) {
    while(left >= 0 && right < str.length() && str[left] == str[right]) {
        left--;
        right++;
    }
    right--;
    left++;
    return right - left + 1;
}
for(int i = 0; i < N; ++i) {
    int left = i, right = i;
    int len1 = expandFromMiddle(str, left, right);
    left = i; right = i + 1;
    int len2 = expandFromMiddle(str, left, right);
}
```
4. O(N) Time | O(N) Space = Manacher's Algorithm

* https://atcoder.jp/contests/dp/tasks/dp_m (Good question on preprocessing)
* https://atcoder.jp/contests/dp/tasks/dp_n (Preprocessing + MCM variation)
* https://leetcode.com/problems/house-robber/submissions/ (Proper use of choice diagram)
* https://leetcode.com/problems/non-negative-integers-without-consecutive-ones/ (DP + Bit Manipulation)


*  https://www.interviewbit.com/problems/longest-valid-parentheses/ (A Different Type Of Problem)
* https://www.geeksforgeeks.org/count-number-ways-reach-given-score-game/ (distinct combinations)
* https://leetcode.com/problems/word-break (String DP)
* https://leetcode.com/problems/word-break-ii (Backtracking + DP)
* https://www.interviewbit.com/problems/deepu-and-girlfriend/ (Game Theory + DP)
when u have 2 players u dont need to maintain the current player in the state 
* https://www.geeksforgeeks.org/maximum-sum-such-that-no-two-elements-are-adjacent/ (House RObber)


* house robber i, ii, iii
* https://www.geeksforgeeks.org/count-number-of-ways-to-divide-a-number-in-4-parts/?ref=lbp
* Weighted job scheduling (IMP)

#### Catalan's Number & Applications =
- Catalan Number = C(n+1) = sigma i=0 to N (C(i) * C(n-i))  = Use DP 
N = 0 -> 1
N = 1 -> 1
N = 2 -> 2
N = 3 -> 5
N = 4 -> 14
N = 5 -> 42
Application = 
1. Finding Nth Catalan Number
2. Find the number of valid parentheses expressions of given length
3. Count No of BSTs that can be formed using N nodes (1 to N numbered) (Notes)
4. Number Of Binary Trees using N nodes labelled from 1 to N
5. Number of unlabelled binary trees
6. Count Binary Tree for a given preorder array of length N


TODO DP =
* buy & sell stock all variations
* https://www.interviewbit.com/problems/n-digit-numbers-with-digit-sum-s-/?ref=random-problem
* https://www.geeksforgeeks.org/count-number-of-ways-to-divide-a-number-in-4-parts/?ref=lbp (only distinct combinations)
* https://www.geeksforgeeks.org/maximize-arrj-arri-arrl-arrk-such-that-i-j-k-l/ 


