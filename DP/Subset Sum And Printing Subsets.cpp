/*
      @author: Charan Sai
*/
#include <bits/stdc++.h>
using namespace std;

#define IOS                           \
    ios_base::sync_with_stdio(false); \
    cin.tie(0);                       \
    cout.tie(0)
#define int long long
#define itr(i, n) for (int i = 0; i < n; ++i)
#define itr1(i, a, n) for (int i = a; i < n; ++i)
#define INF 1e9 + 5
#define pb push_back
#define ppi pair<int, int>
#define endl "\n"
#define nl cout << "\n"
// For Debugging
#define deb(x) cout << #x << " " << x << endl;

void display(const vector<int> &v) {
    for (int i = 0; i < v.size(); ++i) printf("%d ", v[i]);
    printf("\n");
}

// Printing all subsets possible
void printUtil(int arr[], vector<vector<int>> &dp, int n, int sum,
               vector<int> subset) {
    if (sum == 0) {
        display(subset);
        return;
    }
    if (n == 0) {
        return;
    }

    // exclude arr[n-1]
    if (dp[n - 1][sum]) {
        printUtil(arr, dp, n - 1, sum, subset);
    }
    // include arr[n-1];
    if (arr[n - 1] <= sum && dp[n - 1][sum - arr[n - 1]]) {
        subset.push_back(arr[n - 1]);
        printUtil(arr, dp, n - 1, sum - arr[n - 1], subset);
    }
}

void printAllSubsets(int arr[], int n, int sum) {
    vector<vector<int>> dp(n + 1, vector<int>(sum + 1));
    itr1(j, 1, sum + 1) dp[0][j] = 0;
    itr(i, n + 1) dp[i][0] = 1;
    //
    itr1(i, 1, n + 1) itr1(j, 1, sum + 1) {
        dp[i][j] = (arr[i - 1] <= j) ? dp[i - 1][j] || dp[i - 1][j - arr[i - 1]]
                                     : dp[i - 1][j];
    }
    if (dp[n][sum])
        printUtil(arr, dp, n, sum, {});
    else
        printf("No subset possible with that sum\n");
}

// Driver code
int32_t main() {
    int arr[] = {1, 2, 3, 4, 5};
    int n = sizeof(arr) / sizeof(arr[0]);
    int sum = 10;
    printAllSubsets(arr, n, sum);
    return 0;
}
