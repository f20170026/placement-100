#include <bits/stdc++.h>
using namespace std;

/**
* Given a string break into segments such that each segment belongs to the given
* dictionary. Need to check if it is possible or not?
*/

class Solution {
   public:
    bool wordBreak(string s, vector<string> &wordDict);
};

unordered_set<string> dict;
unordered_map<int, bool> cache;  // index->result

// cache[ind] = is it possible to break the word [ind....N-1]
bool wordBreakRecursive(string s, int ind = 0) {
    if (ind >= s.length()) {
        return true;
    }

    if (cache.find(ind) != cache.end()) {
        return cache[ind];
    }

    string prefix = "";
    for (int i = ind; i < s.length(); ++i) {
        prefix += s[i];
        if (dict.find(prefix) != dict.end()) {
            if (wordBreakRecursive(s, i + 1)) {
                return cache[ind] = true;
            }
        }
    }
    return cache[ind] = false;
}

bool Solution::wordBreak(string s, vector<string> &wordDict) {
    dict.clear();
    for (string s : wordDict) {
        dict.insert(s);
    }
    cache.clear();
    return wordBreakRecursive(s);
}