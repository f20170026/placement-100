#include <bits/stdc++.h>
using namespace std;

/**
 * Same as 3. Word Break 2
 */

bool isPalindrome(string str) {
    string reversed = str;
    reverse(reversed.begin(), reversed.end());
    return (str == reversed);
}

vector<vector<string>> backtrack(string A, int ind,
                                 vector<vector<string>> cache[]) {
    if (ind >= A.length()) {
        return {};
    }

    if (!cache[ind].empty()) {
        return cache[ind];
    }

    string word = "";
    vector<vector<string>> ans;
    for (int k = ind; k < A.length(); ++k) {
        word += A[k];
        //Instead of this O(N) function to find palindrome you can preprocess all palindromic substrings
        if (isPalindrome(word)) {
            vector<vector<string>> subproblemResult =
                backtrack(A, k + 1, cache);
            for (vector<string> arr : subproblemResult) {
                arr.insert(arr.begin(), word);
                ans.push_back(arr);
            }
            // it is definitely possible to partition any string into
            // palindromes. so k == A.length() -1  is not needed since
            // subproblemresult will be empty when k == A.length() - 1 only
            if (subproblemResult.empty() && k == A.length() - 1) {
                ans.push_back({word});
            }
        }
    }
    return (cache[ind] = ans);
}

vector<vector<string>> partition(string A) {
    vector<vector<string>> cache[A.size()];
    return backtrack(A, 0, cache);
}